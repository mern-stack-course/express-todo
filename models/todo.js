let idCount = 0;
function Todo(description) {
  return {
    id: ++idCount,
    description: description,
  };
}

module.exports.Todo = Todo;
