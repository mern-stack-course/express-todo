const service = require("../service/todo");
function getTodoHandler(req, res) {
  const todos = service.getTodos();
  res.send(todos);
}

function getSingleTodoHandler(req, res) {
  const todoIDString = req.params.todoID;
  const todoID = parseInt(todoIDString);
  const todo = service.getTodo(todoID);
  res.send(todo);
}

function createTodoHandler(req, res) {
  service.createTodo(req.body.description);
  res.send("created todo");
}

function deleteTodoHandler(req, res) {
  const todoIDString = req.params.todoID;
  const todoID = parseInt(todoIDString);

  const isDeleted = service.deleteTodo(todoID);
  if (isDeleted) {
    res.send("todo deleted");
  } else {
    res.send("couldn't delete todo");
  }
}
module.exports = {
  getTodoHandler: getTodoHandler,
  getSingleTodoHandler: getSingleTodoHandler,
  createTodoHandler: createTodoHandler,
  deleteTodoHandler: deleteTodoHandler,
};
