const todoRepo = require("../repository/todo");
const model = require("../models/todo");
function getTodos() {
  //get all todos
  return todoRepo.getTodos();
}

function getTodo(todoId) {
  //get single todo based on ID
  return todoRepo.getTodo(todoId);
}

function createTodo(description) {
  //update Todo based on id and set description
  const todo = model.Todo(description);
  todoRepo.createTodo(todo);
}

function deleteTodo(todoID) {
  //delete todo based on ID
  return todoRepo.deleteTodo(todoID);
}

function updateTodo(todoID, description) {
  //update Todo based on id and set description
  todoRepo.updateTodo(todoID, description);
}

module.exports = {
  createTodo: createTodo,
  getTodos: getTodos,
  getTodo: getTodo,
  updateTodo: updateTodo,
  deleteTodo: deleteTodo,
};
