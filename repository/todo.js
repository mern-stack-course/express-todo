const todos = [];

function createTodo(todo) {
  todos.push(todo);
}

function getTodos() {
  return todos;
}

function getTodo(todoID) {
  for (let i = 0; i < todos.length; i++) {
    if (todos[i].id === todoID) {
      return todos[i];
    }
  }
  return null;
}

function updateTodo(todoID, description) {
  for (let i = 0; i < todos.length; i++) {
    if (todos[i].id === todoID) {
      todos[i].description = description;
    }
  }
}

function deleteTodo(todoID) {
  let matchingTodoIndex;
  for (let i = 0; i < todos.length; i++) {
    if (todos[i].id === todoID) {
      matchingTodoIndex = i;
    }
  }
  if (matchingTodoIndex == undefined) {
    return false;
  }
  console.log("matching index:", matchingTodoIndex);
  console.log("todo before", todos);
  todos.splice(matchingTodoIndex, 1);
  console.log("todo after", todos);
  return true;
}

module.exports = {
  createTodo: createTodo,
  getTodos: getTodos,
  getTodo: getTodo,
  updateTodo: updateTodo,
  deleteTodo: deleteTodo,
};
