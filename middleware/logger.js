function logger(req, res, next) {
  console.log("received request", req.method, req.url);
  next();
}

module.exports = logger;
