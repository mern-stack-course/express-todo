const express = require("express");

const handler = require("./handlers/todo");
const logger = require("./middleware/logger");
const app = express();
app.use(express.urlencoded());
app.use(logger);

app.get("/todos", handler.getTodoHandler);
app.get("/todos/:todoID", handler.getSingleTodoHandler);
app.post("/todos", handler.createTodoHandler);
app.delete("/todos/:todoID", handler.deleteTodoHandler);

app.listen(3000);
